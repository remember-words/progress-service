package ru.vbotalov.progressservice;

import ru.vbotalov.progressservice.entities.WordProgress;

public final class TestData {

    public static final int USER_ID = 10_000;

    public static final int TOTAL_ANSWERS = 135;
    public static final int CORRECT_ANSWERS = 129;

    public static WordProgress getTestProgress() {
        var progress = new WordProgress();
        progress.setUserId(USER_ID);
        progress.setTotal(TOTAL_ANSWERS);
        progress.setCorrect(CORRECT_ANSWERS);
        return progress;
    }

}
