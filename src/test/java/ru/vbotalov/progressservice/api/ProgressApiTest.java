package ru.vbotalov.progressservice.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import ru.vbotalov.progressservice.api.requests.UpdateProgressRequest;
import ru.vbotalov.progressservice.services.RateLimiterService;
import ru.vbotalov.progressservice.services.WordProgressService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static ru.vbotalov.progressservice.TestData.USER_ID;

public class ProgressApiTest {

    public static final int USER_LEVEL = 5;

    private ProgressApi api;

    private WordProgressService wordProgressService;
    private RateLimiterService rateLimiterService;

    @BeforeEach
    public void before() {
        wordProgressService = mock(WordProgressService.class);
        rateLimiterService = mock(RateLimiterService.class);

        api = new ProgressApi(wordProgressService, rateLimiterService);
    }

    @Test
    public void shouldGetProgressSuccess() {
        when(wordProgressService.getUserLevel(USER_ID)).thenReturn(USER_LEVEL);
        when(rateLimiterService.isNotLimitedRead()).thenReturn(true);

        var responseEntity = api.getProgress(USER_ID);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        var response = responseEntity.getBody();

        assertTrue(response.isSuccess());
        assertEquals(USER_LEVEL, response.getLevel());
    }

    @Test
    public void shouldGetProgressFailedOverLimit() {
        when(wordProgressService.getUserLevel(USER_ID)).thenReturn(USER_LEVEL);
        when(rateLimiterService.isNotLimitedRead()).thenReturn(false);

        var responseEntity = api.getProgress(USER_ID);
        assertEquals(HttpStatus.TOO_MANY_REQUESTS, responseEntity.getStatusCode());
    }

    @Test
    public void shouldUpdateProgressSuccess() {
        when(rateLimiterService.isNotLimitedWrite()).thenReturn(true);

        var request = new UpdateProgressRequest();
        request.setUserId(USER_ID);
        request.setCorrect(true);

        var responseEntity = api.updateProgress(request);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        var response = responseEntity.getBody();
        assertTrue(response.isSuccess());
        verify(wordProgressService).updateProgress(USER_ID, true);
    }

    @Test
    public void shouldUpdateProgressFailedOverLimit() {
        when(rateLimiterService.isNotLimitedWrite()).thenReturn(false);

        var request = new UpdateProgressRequest();
        request.setUserId(USER_ID);
        request.setCorrect(true);

        var responseEntity = api.updateProgress(request);
        assertEquals(HttpStatus.TOO_MANY_REQUESTS, responseEntity.getStatusCode());
    }

}
