package ru.vbotalov.progressservice.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.vbotalov.progressservice.repositories.WordProgressRepository;
import ru.vbotalov.progressservice.services.stored.StoredWordProgressService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;
import static ru.vbotalov.progressservice.TestData.*;

public class StoredWordProgressServiceTest {

    private WordProgressService service;

    private WordProgressRepository repository;

    @BeforeEach
    public void before() {
        repository = mock(WordProgressRepository.class);

        service = new StoredWordProgressService(repository);
    }

    @Test
    public void shouldGetLevelSuccess() {
        when(repository.findByUserId(USER_ID)).thenReturn(
                Optional.of(getTestProgress())
        );

        var level = service.getUserLevel(USER_ID);
        assertEquals(6, level);
    }

    @Test
    public void shouldUpdateCorrectAnswerSuccess() {
        when(repository.findByUserId(USER_ID)).thenReturn(
                Optional.of(getTestProgress())
        );

        service.updateProgress(USER_ID, true);

        verify(repository).save(argThat(progress ->
                progress.getTotal() == TOTAL_ANSWERS + 1
                        && progress.getCorrect() == CORRECT_ANSWERS + 1));
    }

    @Test
    public void shouldUpdateWrongAnswerSuccess() {
        when(repository.findByUserId(USER_ID)).thenReturn(
                Optional.of(getTestProgress())
        );

        service.updateProgress(USER_ID, false);

        verify(repository).save(argThat(progress ->
                progress.getTotal() == TOTAL_ANSWERS + 1
                        && progress.getCorrect() == CORRECT_ANSWERS));
    }

}
