package ru.vbotalov.progressservice.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.vbotalov.progressservice.config.RateAllLimitProperties;
import ru.vbotalov.progressservice.config.RateLimitProperties;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RateLimiterServiceTest {

    private RateLimiterService service;

    @BeforeEach
    public void before() {
        RateAllLimitProperties allProperties = new RateAllLimitProperties();
        RateLimitProperties readProperties = new RateLimitProperties();
        readProperties.setCount(1);
        readProperties.setTokens(1);
        readProperties.setPeriod(1);
        RateLimitProperties writeProperties = new RateLimitProperties();
        writeProperties.setCount(2);
        writeProperties.setTokens(1);
        writeProperties.setPeriod(1);
        allProperties.setRead(readProperties);
        allProperties.setWrite(writeProperties);

        service = new RateLimiterService(allProperties);
    }

    @Test
    public void shouldNotBeLimitedForRead() {
        assertTrue(service.isNotLimitedRead());
    }

    @Test
    public void shouldBeLimitedRead() {
        assertTrue(service.isNotLimitedRead());
        assertFalse(service.isNotLimitedRead());
    }

    @Test
    public void shouldNotBeLimitedForWrite() {
        assertTrue(service.isNotLimitedWrite());
        assertTrue(service.isNotLimitedWrite());
    }

    @Test
    public void shouldBeLimitedForWrite() {
        assertTrue(service.isNotLimitedWrite());
        assertTrue(service.isNotLimitedWrite());
        assertFalse(service.isNotLimitedWrite());
    }

}
