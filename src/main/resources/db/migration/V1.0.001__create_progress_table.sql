create table words_progress
(
    user_id         integer           not null
        constraint words_progress_pk
            primary key,
    total_answers   integer default 0 not null,
    correct_answers integer default 0 not null
);

comment on table words_progress is 'Прогресс изучения слов';
comment on column words_progress.user_id is 'Идентификатор пользователя';
comment on column words_progress.total_answers is 'Всего ответов';
comment on column words_progress.correct_answers is 'Правильных ответов';
