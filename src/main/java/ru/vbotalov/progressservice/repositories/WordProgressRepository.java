package ru.vbotalov.progressservice.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.vbotalov.progressservice.entities.WordProgress;

import java.util.Optional;

public interface WordProgressRepository extends CrudRepository<WordProgress, Integer> {

    Optional<WordProgress> findByUserId(Integer userId);

}
