package ru.vbotalov.progressservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan(basePackages = "ru.vbotalov.progressservice.config")
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
