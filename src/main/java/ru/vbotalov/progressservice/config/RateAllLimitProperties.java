package ru.vbotalov.progressservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("app.rate-limit")
public class RateAllLimitProperties {

    private RateLimitProperties read;
    private RateLimitProperties write;

    public RateLimitProperties getRead() {
        return read;
    }

    public void setRead(RateLimitProperties read) {
        this.read = read;
    }

    public RateLimitProperties getWrite() {
        return write;
    }

    public void setWrite(RateLimitProperties write) {
        this.write = write;
    }
}
