package ru.vbotalov.progressservice.services;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Refill;
import org.springframework.stereotype.Service;
import ru.vbotalov.progressservice.config.RateAllLimitProperties;

import java.time.Duration;

@Service
public class RateLimiterService {

    private final static int TOKEN_COUNT = 1;

    private final Bucket readBucket;
    private final Bucket writeBucket;

    public RateLimiterService(RateAllLimitProperties rateLimitProperties) {
        Bandwidth readLimit = Bandwidth.classic(
                rateLimitProperties.getRead().getCount(), Refill.greedy(
                        rateLimitProperties.getRead().getTokens(),
                        Duration.ofSeconds(rateLimitProperties.getRead().getPeriod())));
        Bandwidth writeLimit = Bandwidth.classic(
                rateLimitProperties.getWrite().getCount(), Refill.greedy(
                        rateLimitProperties.getWrite().getTokens(),
                        Duration.ofSeconds(rateLimitProperties.getWrite().getPeriod())));
        this.readBucket = Bucket.builder()
                .addLimit(readLimit)
                .build();
        this.writeBucket = Bucket.builder()
                .addLimit(writeLimit)
                .build();
    }

    public boolean isNotLimitedRead() {
        return readBucket.tryConsume(TOKEN_COUNT);
    }

    public boolean isNotLimitedWrite() {
        return writeBucket.tryConsume(TOKEN_COUNT);
    }

}
