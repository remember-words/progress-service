package ru.vbotalov.progressservice.services;

public interface WordProgressService {

    int getUserLevel(int userId);

    void updateProgress(int userId, boolean correct);

}
