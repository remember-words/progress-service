package ru.vbotalov.progressservice.services.stored;

import org.springframework.stereotype.Service;
import ru.vbotalov.progressservice.entities.WordProgress;
import ru.vbotalov.progressservice.repositories.WordProgressRepository;
import ru.vbotalov.progressservice.services.WordProgressService;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class StoredWordProgressService implements WordProgressService {

    private static final int MAX_LEVEL = 30;
    private static final int LEVEL_THRESHOLD = 80;
    private static final Map<Integer, Integer> LEVELS;

    private final WordProgressRepository repository;

    public StoredWordProgressService(WordProgressRepository repository) {
        this.repository = repository;
    }

    @Override
    public int getUserLevel(int userId) {
        var progress = repository.findByUserId(userId).orElseGet(() -> {
            var newProgress = new WordProgress();
            newProgress.setUserId(userId);
            newProgress.setTotal(0);
            newProgress.setCorrect(0);
            newProgress.setNewEntity(true);
            repository.save(newProgress);
            return newProgress;
        });
        return getCalculatedLevel(progress);
    }

    @Override
    public void updateProgress(int userId, boolean correct) {
        var progress = repository.findByUserId(userId).orElseThrow(() -> new IllegalArgumentException("Unknown user"));
        progress.setTotal(progress.getTotal() + 1);
        if (correct) {
            progress.setCorrect(progress.getCorrect() + 1);
        }
        repository.save(progress);
    }

    private int getCalculatedLevel(WordProgress progress) {
        var reachedLevels = LEVELS.entrySet().stream()
                .filter(entry -> {
                    if (progress.getTotal() == 0 || progress.getTotal() < entry.getValue()) {
                        return false;
                    }
                    var percent = progress.getCorrect() * 100 / progress.getTotal();
                    return percent >= LEVEL_THRESHOLD;
                })
                .map(Map.Entry::getKey)
                .toList();
        return reachedLevels.isEmpty() ? 0 : reachedLevels.get(reachedLevels.size() - 1);
    }

    static {
        LEVELS = new LinkedHashMap<>();
        int value = 2;
        for (int i = 0; i < MAX_LEVEL; i++) {
            LEVELS.put(i, value);
            value *= 2;
        }
    }
}
