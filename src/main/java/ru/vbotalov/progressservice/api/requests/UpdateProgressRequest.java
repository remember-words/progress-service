package ru.vbotalov.progressservice.api.requests;

public class UpdateProgressRequest {

    private int userId;
    private boolean correct;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }
}
