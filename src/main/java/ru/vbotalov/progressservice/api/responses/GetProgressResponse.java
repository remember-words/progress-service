package ru.vbotalov.progressservice.api.responses;

public class GetProgressResponse extends DefaultResponse {

    private final int level;

    public GetProgressResponse(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
