package ru.vbotalov.progressservice.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.vbotalov.progressservice.api.requests.UpdateProgressRequest;
import ru.vbotalov.progressservice.api.responses.DefaultResponse;
import ru.vbotalov.progressservice.api.responses.GetProgressResponse;
import ru.vbotalov.progressservice.services.RateLimiterService;
import ru.vbotalov.progressservice.services.WordProgressService;

@RestController
@RequestMapping(path = "/api/v1/progress")
public class ProgressApi {

    private final WordProgressService progressService;
    private final RateLimiterService rateLimiterService;

    public ProgressApi(
            WordProgressService progressService,
            RateLimiterService rateLimiterService) {
        this.progressService = progressService;
        this.rateLimiterService = rateLimiterService;
    }

    @GetMapping(path = "/{userId}")
    public ResponseEntity<GetProgressResponse> getProgress(@PathVariable int userId) {
        if (rateLimiterService.isNotLimitedRead()) {
            return ResponseEntity.ok(new GetProgressResponse(progressService.getUserLevel(userId)));
        }
        return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
    }

    @PostMapping(path = "/update")
    public ResponseEntity<DefaultResponse> updateProgress(@RequestBody UpdateProgressRequest request) {
        if (!rateLimiterService.isNotLimitedWrite()) {
            return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
        }
        progressService.updateProgress(request.getUserId(), request.isCorrect());
        return ResponseEntity.ok(new DefaultResponse());
    }

}
